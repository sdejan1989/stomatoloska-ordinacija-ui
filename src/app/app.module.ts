import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { PatientsComponent } from './patients/patients.component';
import { AddingComponent } from './patients/adding/adding.component';
import { ListComponent } from './patients/list/list.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {MainComponent} from './main/main.component';
import {AppRoutingModule} from './app-routing.module';
import {TeethComponent} from './patients/adding/teeth/teeth.component';
import {BasicDataComponent} from './patients/adding/basic-data/basic-data.component';
import {ToothComponent} from './patients/adding/teeth/tooth/tooth.component';
import {JawComponent} from './patients/adding/teeth/jaw/jaw.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    PatientsComponent,
    AddingComponent,
    ListComponent,
    MainComponent,
    TeethComponent,
    BasicDataComponent,
    ToothComponent,
    JawComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
    AppRoutingModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
