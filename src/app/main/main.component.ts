import {Component} from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent {
  images =  ['assets/image1.jpg', 'assets/image2.jpg', 'assets/image3.jpg'];
}
