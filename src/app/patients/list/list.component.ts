import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/internal/Observable';
import { PersonService } from '../adding/basic-data/person.service';
import { Patient } from '../patient.model';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  @Input()
  patientFormGroup: FormGroup;

  @Output()
  deleteEvent: EventEmitter<void> = new EventEmitter();

  data: Observable<any[]>;

  constructor(private personService: PersonService) { }

  ngOnInit(): void {
    this.data = this.personService.getAll();

  }

  delete(id){
    // for(let i = 0; i < this.data.length; ++i){
    //     if (this.data[i].id === id) {
    //         this.data.splice(i,1);
    //     }
    // }
}
}
