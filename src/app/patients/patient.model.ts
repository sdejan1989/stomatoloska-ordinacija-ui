export interface Patient {
    id: string;
    firstName: string;
    lastName: string;
    birthDate: string;
    city: string;
    country: string;
    phoneNumber: string;
}
