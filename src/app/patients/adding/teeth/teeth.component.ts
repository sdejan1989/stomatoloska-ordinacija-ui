import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-teeth',
  templateUrl: './teeth.component.html',
  styleUrls: ['./teeth.component.scss']
})
export class TeethComponent implements OnInit {
  @Input()
  public patientFormGroup: FormGroup;

  @Output()
  saveEvent: EventEmitter<void> = new EventEmitter();

  constructor(private formBuilder: FormBuilder) {
  }

   ngOnInit(): void {
    this.patientFormGroup.addControl(
      'teeth',
      this.formBuilder.group({})
    );
  }

  save(): void {
    this.saveEvent.emit();
  }
  
  get teeth(): FormGroup {
    return this.patientFormGroup.get('teeth') as FormGroup;
  }


}
