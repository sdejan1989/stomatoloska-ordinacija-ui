import {Component, Input, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-jaw',
  templateUrl: './jaw.component.html',
  styleUrls: ['./jaw.component.scss']
})
export class JawComponent implements OnInit {

  @Input()
  public jawFormGroup: FormGroup;

  constructor(private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    const formGroup = this.jawFormGroup;
    formGroup.addControl('upperLeft', this.formBuilder.group({
      tooth: this.formBuilder.array([])
    }));
    formGroup.addControl('upperRight', this.formBuilder.group({
      tooth: this.formBuilder.array([])
    }));
    formGroup.addControl('lowerLeft', this.formBuilder.group({
      tooth: this.formBuilder.array([])
    }));
    formGroup.addControl('lowerRight', this.formBuilder.group({
      tooth: this.formBuilder.array([])
    }));

    ['upperLeft', 'upperRight', 'lowerLeft', 'lowerRight'].map(jaw => {
      for (let i = 0; i < 8; i++) {
        (this.jawFormGroup.get(jaw).get('tooth') as FormArray).push(this.formBuilder.group({
          healthy: false,
          toothDecay: false,
          repaired: false,
          extracted: false
        }));
      }
    });
  }

}
