import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-tooth',
  templateUrl: './tooth.component.html',
  styleUrls: ['./tooth.component.scss']
})
export class ToothComponent implements OnInit {

  @Input()
  i: number;

  @Input()
  toothFormGroup: FormGroup;

  constructor() { }

  ngOnInit(): void {
  }

  stateOfTeeth(): void {
    const tooth = this.toothFormGroup;
    if ((tooth.get('healthy').value && tooth.get('repaired').value) || tooth.get('healthy').value) {
      tooth.get('toothDecay').disable();
      tooth.get('extracted').disable();
    } else if ((tooth.get('repaired').value && tooth.get('toothDecay').value)
      || tooth.get('toothDecay').value || tooth.get('repaired').value) {
      tooth.get('healthy').disable();
      tooth.get('extracted').disable();
    } else if (tooth.get('extracted').value) {
      tooth.get('healthy').disable();
      tooth.get('toothDecay').disable();
      tooth.get('repaired').disable();
    } else {
      tooth.get('healthy').enable();
      tooth.get('toothDecay').enable();
      tooth.get('repaired').enable();
      tooth.get('extracted').enable();
    }
  }
}
