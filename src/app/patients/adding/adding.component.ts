import { Component} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Router} from '@angular/router';
import { from } from 'rxjs';
import { PersonService, Person } from './basic-data/person.service';



@Component({
  selector: 'app-adding',
  templateUrl: './adding.component.html',
  styleUrls: ['./adding.component.scss']
})

export class AddingComponent {
  currentPage = 'BASIC_DATA';
  patientFormGroup = new FormGroup({});

  constructor(private router: Router, private personService: PersonService) {
  }

  goToHomePage(): void {
    // console.log(this.patientFormGroup.value);
    this.router.navigate(['/home']);
    this.onSubmit();
  }

  onSubmit(): void {
    // console.log(this.patientFormGroup.get('basicData').valid);
    if (this.patientFormGroup.valid) {
      // console.log(this.patientFormGroup.value);
      this.basicDataAdded(this.patientFormGroup.value);
    } else {
      this.patientFormGroup.markAllAsTouched();
    }
  }

    basicDataAdded(person: Person): void {
      // console.log(this.patientFormGroup.value);
      this.personService.add(this.patientFormGroup.value)
        .subscribe((returnedBasicData: Person) => {
          console.log('saved')
        });
    }
}
