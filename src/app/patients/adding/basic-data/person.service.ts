import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import { Patient } from '../../patient.model';

export interface Person {
    basicData: {
      id?: number;
      firstName: string;
      lastName: string;
      birthDate: string;
      city: string;
      country: string;
      phoneNumber: string;
    },
    teeth: {
      upperLeft: {
        tooth: {
          id: number;
          healthy: string;
          toothDecay: string;
          repaired: string;
          extracted: string;
        }[];
      },
      upperRight: {
        tooth: {
          id:number;
          healthy: string;
          toothDecay: string;
          repaired: string;
          extracted: string;
        }[];
      },
      lowerLeft: {
        tooth: {
          id: number;
          healthy: string;
          toothDecay: string;
          repaired: string;
          extracted: string;
        }[];
      },
      lowerRight: {
        tooth: {
          id:number;
          healthy: string;
          toothDecay: string;
          repaired: string;
          extracted: string;
        }[];
      };
    };
}

@Injectable({
  providedIn: 'root'
})
export class PersonService {

  private personUrl = 'http://localhost:8080/person';

  constructor(private http: HttpClient) {
  }

  add(person: Person): Observable<object> {
    return this.http.post(
      this.personUrl,
      person
    );
  }

  getAll(): Observable<any> {
    return this.http.get(
      this.personUrl
    );
  }

  delete(id: number): Observable<object> {
    return this.http.delete(
      this.personUrl + '/' + id
    );
  }

  getOne(id: number): Observable<object> {
    return this.http.get(
      this.personUrl + '/' + id
    );
  }

  edit(id: number, editedBasicData: Person): Observable<object> {
    return this.http.put(
      this.personUrl + '/' + id,
      editedBasicData
    );
  }


}
