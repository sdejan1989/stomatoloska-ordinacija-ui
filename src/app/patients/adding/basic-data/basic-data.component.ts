import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NgbDateAdapter} from '@ng-bootstrap/ng-bootstrap';
import {CustomAdapter} from './calendar.service';
import {Person, PersonService} from './person.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-basic-data',
  templateUrl: './basic-data.component.html',
  styleUrls: ['./basic-data.component.scss'],
  providers: [
    {provide: NgbDateAdapter, useClass: CustomAdapter}
  ]
})
export class BasicDataComponent implements OnInit {

  @Input()
  patientFormGroup: FormGroup;

  @Output()
  nextPageEvent: EventEmitter<void> = new EventEmitter();

  model: string;

  constructor(private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.patientFormGroup.addControl(
      'basicData',
      this.formBuilder.group({
          firstName: ['', [Validators.required, Validators.minLength(2), Validators.pattern('[a-zA-Z ]*')]],
          lastName: ['', [Validators.required, Validators.minLength(2), Validators.pattern('[a-zA-Z ]*')]],
          birthDate: [null, [Validators.required, Validators.pattern('((((\\b[1-9]\\b|1[0-9]|2[0-8])[-]([1-9]|1[0-2]))|((29|30|31)[-]([13578]|1[02]))|((29|30)[-]([469]|11)))[-]([0-9][0-9][0-9][0-9]))|(29[-]2[-](([0-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)))')]],
          city: ['', [Validators.required, Validators.minLength(2), Validators.pattern('[a-zA-Z ]*')]],
          country: ['', [Validators.required, Validators.minLength(2), Validators.pattern('[a-zA-Z ]*')]],
          phoneNumber: ['', [Validators.required, Validators.pattern('[0-9]{3} [0-9]{3} [0-9]{3}')]]
        },
        {updateOn: 'blur'}
      ));
  }

  goNextPage(): void {
    if (this.basicData.valid) {
      this.nextPageEvent.emit();
    } else {
      Object.keys(this.basicData.controls).forEach(field => {
        const control = this.basicData.get(field);
        control.markAsTouched({onlySelf: true});
      });
    }
  }

  get basicData(): FormGroup {
    return this.patientFormGroup.get('basicData') as FormGroup;
  }
  get firstName(): FormControl {
    return this.basicData.get('firstName') as FormControl;
  }

  get lastName(): FormControl {
    return this.basicData.get('lastName') as FormControl;
  }

  get birthDate(): FormControl {
    return this.basicData.get('birthDate') as FormControl;
  }

  get city(): FormControl {
    return this.basicData.get('city') as FormControl;
  }

  get country(): FormControl {
    return this.basicData.get('country') as FormControl;
  }

  get phoneNumber(): FormControl {
    return this.basicData.get('phoneNumber') as FormControl;
  }

  // onSubmit(): void {
  //   if (this.basicData.valid) {
  //     // this.basicDataAdded(this.basicData.value);
  //   } else {
  //     this.basicData.markAllAsTouched();
  //   }
  // }

//   basicDataAdded(basicData: BasicData): void {
//     this.basicDataService.add(this.basicData.value)
//       .subscribe((returnedBasicData: BasicData) => {
//         console.log('saved')
//       });
//   }

}
