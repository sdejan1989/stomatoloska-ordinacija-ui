import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AddingComponent} from './patients/adding/adding.component';
import {ListComponent} from './patients/list/list.component';
import {MainComponent} from './main/main.component';

const routes: Routes = [
  {path: 'home', component: MainComponent},
  {path: 'add', component: AddingComponent},
  {path: 'list', component: ListComponent},
  {path: '', redirectTo: '/home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
