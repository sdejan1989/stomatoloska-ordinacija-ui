const express = require('express')
const app = express()

app.use(express.static('./dist/stomatoloska-ordinacija-ui'));

app.get('/*', function (req, res) {
  res.sendFile('index.html', {root: 'dist/stomatoloska-ordinacija-ui/'}
  );
});

app.listen(process.env.PORT || 8080);
